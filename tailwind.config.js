module.exports = {
    mode: "jit",
    purge: ["./build/*.html", "./src/**/*.tsx", "./safeclasses.txt"],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {},
        fontFamily: {
            work: ["Work Sans", "sans-serif"],
            rubik: ["Rubik", "sans-serif"],
            corben: ["Corben", "serif"],
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
