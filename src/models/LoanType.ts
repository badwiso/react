export interface LoanType {
    id: number
    name: string
    interest: number
    min_amount: number
    max_amount: number
    min_tenure: number
    max_tenure: number
    image: string
}
