import React from "react"
import { LoanType } from "src/models/LoanType"

const Month = ({
    item,
    setAmount,
    amount,
}: {
    item: Partial<LoanType>
    setAmount: any
    amount: number
}) => {
    const handleKey = (e: any) => {
        if (e.key === "ArrowUp") setAmount(Math.min(amount + 1, item.max_tenure!))
        if (e.key === "ArrowDown") setAmount(Math.max(amount - 1, item.min_tenure!))
    }
    const acceptNumbers = (e: any) => {
        const re = /^[0-9\b]+$/

        if (e.target.value === "" || re.test(e.target.value)) {
            setAmount(e.target.value)
        }
    }
    const onBlur = (e: any) => {
        const amount = Number.parseFloat(e.target.value)
        const max = item.max_amount!
        const min = item.min_amount!
        if (amount > max) {
            setAmount(max)
        } else if (amount < min) {
            setAmount(min)
        }
    }
    const dec = () => setAmount(Math.max(amount - 1, item.min_tenure!))
    const inc = () => {
        setAmount(Math.min(amount + 1, item.max_tenure!))
    }
    return (
        <div className="h-[56px] rounded-[4px] border-[#E9EEF2] border-[1px] border-solid mx-[24px] flex justify-start">
            <button
                onClick={dec}
                className="my-[8px] ml-[20px] mr-[24px] text-[#CBD5DC] text-[24px] font-corben">
                &lt;
            </button>
            <input
                value={amount || item.min_tenure || "0"}
                min={item.min_tenure}
                max={item.max_tenure}
                onChange={(e) => setAmount(e.target.value)}
                type="number"
                onKeyDown={handleKey}
                className="text-center my-[16px] w-[80%] font-[400] text-[14px] leading-[21px] text-[#4D6475] font-work"
            />
            <button
                onClick={inc}
                className="my-[8px] ml-[20px] mr-[24px] text-[#CBD5DC] text-[24px] font-corben">
                &gt;
            </button>
        </div>
    )
}

export default Month
