import React from "react"
import { LoanType } from "src/models/LoanType"
interface ICONS_TYPE {
    car: JSX.Element
    cash: JSX.Element
    house: JSX.Element
}
const Icon = ({
    item,
    handle,
    selected,
}: {
    item: LoanType
    handle: any
    selected: Partial<LoanType>
}) => {
    const car = (
        <svg
            className="fill-[white] h-[56px] w-[56px] mx-auto"
            xmlns="http://www.w3.org/2000/svg"
            height="24px"
            viewBox="0 0 24 24"
            width="24px"
            fill="#000000">
            <path d="M0 0h24v24H0z" fill="none" />
            <path d="M18.92 6.01C18.72 5.42 18.16 5 17.5 5h-11c-.66 0-1.21.42-1.42 1.01L3 12v8c0 .55.45 1 1 1h1c.55 0 1-.45 1-1v-1h12v1c0 .55.45 1 1 1h1c.55 0 1-.45 1-1v-8l-2.08-5.99zM6.5 16c-.83 0-1.5-.67-1.5-1.5S5.67 13 6.5 13s1.5.67 1.5 1.5S7.33 16 6.5 16zm11 0c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zM5 11l1.5-4.5h11L19 11H5z" />
        </svg>
    )
    const house = (
        <svg
            className="fill-[white] h-[56px] w-[56px] mx-auto"
            xmlns="http://www.w3.org/2000/svg"
            enableBackground="new 0 0 24 24"
            height="24px"
            viewBox="0 0 24 24"
            width="24px"
            fill="#000000">
            <g>
                <rect fill="none" height="24" width="24" />
            </g>
            <g>
                <path d="M19,9.3V4h-3v2.6L12,3L2,12h3v8h5v-6h4v6h5v-8h3L19,9.3z M10,10c0-1.1,0.9-2,2-2s2,0.9,2,2H10z" />
            </g>
        </svg>
    )
    const cash = (
        <svg
            className="fill-[white] h-[56px] w-[56px] mx-auto"
            xmlns="http://www.w3.org/2000/svg"
            height="24px"
            viewBox="0 0 24 24"
            width="24px"
            fill="#000000">
            <path d="M0 0h24v24H0V0z" fill="none" />
            <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm.31-8.86c-1.77-.45-2.34-.94-2.34-1.67 0-.84.79-1.43 2.1-1.43 1.38 0 1.9.66 1.94 1.64h1.71c-.05-1.34-.87-2.57-2.49-2.97V5H10.9v1.69c-1.51.32-2.72 1.3-2.72 2.81 0 1.79 1.49 2.69 3.66 3.21 1.95.46 2.34 1.15 2.34 1.87 0 .53-.39 1.39-2.1 1.39-1.6 0-2.23-.72-2.32-1.64H8.04c.1 1.7 1.36 2.66 2.86 2.97V19h2.34v-1.67c1.52-.29 2.72-1.16 2.73-2.77-.01-2.2-1.9-2.96-3.66-3.42z" />
        </svg>
    )
    const handleClick = () => {
        if (item.id !== selected.id) handle(item)
    }
    const ICONS: ICONS_TYPE = {
        car,
        cash,
        house,
    }
    return (
        <div
            className={
                "rounded-[50%] h-[82px] w-[82px] mx-[2px] flex items-center " +
                (item.id === selected.id ? "bg-[blue] " : "bg-[black]")
            }
            onClick={handleClick}>
            {ICONS[item.image as keyof ICONS_TYPE]}
        </div>
    )
}

export default Icon
