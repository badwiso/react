import React from "react"
import { LoanType } from "src/models/LoanType"

const Amount = ({
    item,
    setAmount,
    amount,
}: {
    item: Partial<LoanType>
    setAmount: any
    amount: number
}) => {
    const handleChange = (e: any) => {
        const amount = e.target.value.replace(",", "")

        if (!amount || amount.match(/^\d{1,}(\.\d{0,3})?$/)) {
            setAmount(amount)
        }
    }
    const handleBlur = (e: any) => {
        const amount = Number.parseFloat(e.target.value.replace(",", ""))
        const max = item.max_amount!
        const min = item.min_amount!
        if (amount > max) {
            setAmount(
                max.toLocaleString("en-US", {
                    minimumFractionDigits: 2,
                })
            )
        } else if (amount < min) {
            setAmount(
                min.toLocaleString("en-US", {
                    minimumFractionDigits: 2,
                })
            )
        } else {
            setAmount(
                amount.toLocaleString("en-US", {
                    minimumFractionDigits: 2,
                })
            )
        }
    }
    const handleKey = (e: any) => {
        const amount = Number.parseFloat(e.target.value.replace(",", ""))
        const max = item.max_amount!
        const min = item.min_amount!
        if (e.key === "ArrowUp")
            setAmount(
                Math.min(amount + 1, max).toLocaleString("en-US", {
                    minimumFractionDigits: 2,
                })
            )
        if (e.key === "ArrowDown")
            setAmount(
                Math.max(amount - 1, min).toLocaleString("en-US", {
                    minimumFractionDigits: 2,
                })
            )
    }
    return (
        <div
            className="h-[56px] rounded-[4px] border-[#E9EEF2] border-[1px] border-solid mx-[24px] flex justify-start"
            onKeyDown={handleKey}>
            <span className="my-[8px] ml-[20px] mr-[24px] text-[#CBD5DC] text-[26px] font-bold">
                $
            </span>
            <input
                value={amount || item.min_amount || "0"}
                min={item.min_amount}
                max={item.max_amount}
                onChange={handleChange}
                onBlur={handleBlur}
                pattern="^\d*(\.\d{0,2})?$"
                className="my-[16px] w-[80%] font-[500] text-[20px] leading-[24px] text-[#4D6475] font-rubik"
            />
        </div>
    )
}

export default Amount
