import React from "react"
import Card from "./Card"

const Calculator = () => (
    <div className="w-full pt-5 h-full flex justify-center flex-col bg-[#E5E5E5] font-work">
        <div className="w-full md:mx-auto  md:w-[560px]  mt-[88px] mb-[28px] mx-[16px] text-[#1B31A8] text-[18px] h-[22px] font-normal leading-[21.6px] text-center">
            Let&apos;s plan your <strong>loan.</strong>
        </div>
        <Card />
    </div>
)

export default Calculator
