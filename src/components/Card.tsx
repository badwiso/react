import React, { useEffect, useState } from "react"
import Amount from "./Amount"
import LoanPicker from "./LoanPicker"
import Month from "./Month"
import Result from "./Result"

const Card = () => {
    const [loan, setLoan] = useState([])
    const [selected, setSelected] = useState({})
    const [amount, setAmount] = useState(0)
    const [month, setMonth] = useState(0)
    const [total, setTotal] = useState(0)
    const [monthly, setMonthly] = useState(0)
    const getData = () => {
        fetch("./products.json").then((response) => {
            response.json().then((data) => {
                data = data.map((item: any) => {
                    return {
                        ...item,
                        id: Number.parseInt(item.id),
                        interest: Number.parseFloat(item.interest),
                        min_amount: Number.parseFloat(item.min_amount),
                        max_amount: Number.parseFloat(item.max_amount),
                        min_tenure: Number.parseInt(item.min_tenure),
                        max_tenure: Number.parseInt(item.max_tenure),
                    }
                })
                setLoan(data)
                if (data.length > 0) {
                    setSelected(data[0])
                    setAmount(data[0].min_amount)
                    setMonth(data[0].min_tenure)
                } else setSelected({})
            })
        })
    }
    useEffect(() => {
        getData()
    }, [])
    useEffect(() => {
        const a = Number.parseFloat(`${amount}`.replace(",", ""))

        const tot = a + (a * (selected as any)!.interest) / 100
        setTotal(tot)
        setMonthly(tot / month)
    }, [month, amount])
    const handle = (item: any) => {
        setSelected(item)
        setAmount(item.min_amount)
        setMonth(item.min_tenure)
    }

    const currency = (e: number) =>
        Number.parseFloat(`${e}`).toLocaleString("en-US", { style: "currency", currency: "USD" })
    return (
        <div className="bg-[white] w-full  md:w-[560px] mx-auto h-full mb-[63px] rounded-[8px]">
            <LoanPicker data={loan} selected={selected} setSelected={handle} />
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 lg:gap-8 m-5">
                <div>
                    <div className=" mt-[8px] ml-[24px] mb-[4px] font-work font-[12px] font-[400] leading-[16px] text-left ">
                        Loan amount
                    </div>
                    <Amount amount={amount} item={selected} setAmount={setAmount} />
                </div>
                <div>
                    <div className="mt-[8px] ml-[24px] mb-[4px] font-work font-[12px] font-[400] leading-[16px] text-left ">
                        Reach goal by
                    </div>
                    <Month amount={month} item={selected} setAmount={setMonth} />
                </div>
            </div>
            <Result
                total={currency(total)}
                monthly={currency(monthly)}
                month={month}
                amount={currency(amount)}
            />
            <div className="mt-[32px] mb-[40px] py-[18px] flex justify-center mx-[20px]">
                <button
                    className="bg-[#1B31A8] p-[18px] w-full md:w-[320px] text-center text-white rounded-[32px] 
             font-work text-[16px] font-semibold leading-[20px] ">
                    Apply Now
                </button>
            </div>
        </div>
    )
}

export default Card
