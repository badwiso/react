import React from "react"
import { LoanType } from "src/models/LoanType"
import Icon from "./Icon"
const LoanPicker = ({
    data,
    selected,
    setSelected,
}: {
    data: LoanType[]
    selected: Partial<LoanType>
    setSelected: any
}) => {
    return (
        <div className="mx-[49px] mt-[10px] h-[86px] flex justify-center">
            {data.map((item: LoanType) => (
                <Icon key={item.id} item={item} handle={setSelected} selected={selected} />
            ))}
        </div>
    )
}

export default LoanPicker
