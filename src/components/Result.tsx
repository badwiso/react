import React from "react"

const Result = ({ total, monthly, amount, month }: any) => {
    const date = (e: number) => {
        let d = new Date()
        d.setMonth(d.getMonth() + e)
        return d.toLocaleString("default", { month: "long", year: "numeric" })
    }
    return (
        <div className="mt-[24px] h-[155px] rounded-[8px] border-[#E9EEF2] border-[1px] border-solid mx-[24px] flex-col">
            <div className="mx-[24px] mt-[24px] flex justify-between">
                <span className="w-[160px] h-[22px] font-work font-[400] font-[18px] leading-[21.6px] text-[#1E2A32]">
                    Monthly amount
                </span>
                <span className="w-[96px] h-[29px] font-rubik font-[24px] text-[#0079FF] leading-[29px] text-right align-text-top">
                    {monthly}
                </span>
            </div>
            <div className=" w-full h-[50%] mt-[18px] bg-[#F4F8FA] py-[16px] px-[32px] ">
                <p className=" font-work font-[12px] font-[400] leading-[16px] text-center ">
                    You’re planning {month} <strong>monthly deposits</strong> to reach your{" "}
                    <strong>{amount}</strong> goal by <strong>{date(month)}</strong>. The total
                    amount loaned will be <strong>{total}</strong>
                </p>
            </div>
        </div>
    )
}

export default Result
