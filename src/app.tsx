import Calculator from "@components/Calculator"
import React from "react"
import ReactDOM from "react-dom"
import "./index.css"

const App = () => <Calculator />

ReactDOM.render(<App />, document.getElementById("root"))
